import numpy as np
import matplotlib.pyplot as plt
from bb_script import *

# Exemple d'une fonction simple à optimiser.
# Dans l'idée, on est pas vraiment au courant de la fonction. 

f = Alpine1(input_dim=1).f

X = np.linspace(-10, 10, 200).reshape(200, -1)

Y = f(X)

plt.plot(X, Y)
plt.title("Exemple d'une fonction simple, on cherche le minimum en un minimum d'evaluation")
plt.xlabel("X")
plt.ylabel("Y")
plt.show()

# Example d'une optimisation aléatoire.
# Cette stratégie donne des informations valuables sur la fonction à optimiser.

model = DirectArylation()
function = model.f
domain = model.domain

nb_evaluations = 100

X = None
Y = None
for i in range(nb_evaluations):
    x = np.array([])
    for var in domain:
        if var['type'] == 'continuous':
            x = np.append(x, np.random.rand())
        elif var['type'] == 'categorical':
            min = var['domain'][0]
            max = var['domain'][len(var['domain'])-1]
            x = np.append(x, np.random.randint(min, max + 1))
    x = x.reshape(1, -1)
    y = function(x)
    y = np.array(y).reshape(-1, 1)
    if X is None:
        X = x
        Y = y
    else:
        X = np.append(X, x, axis=0)
        Y = np.append(Y, y, axis=0)

curr_best = 1000
best_y = []
for i in range(nb_evaluations):
    curr_best = np.minimum(curr_best, Y[i, :])
    best_y.append(curr_best)

plt.plot(best_y)
plt.title("Evolution du meilleur score obtenu (minimisation)")
plt.xlabel("Iterations")
plt.ylabel("Meilleur score")
plt.show()
