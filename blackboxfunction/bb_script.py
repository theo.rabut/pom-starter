
import pickle
import numpy as np
savings_path = 'regressor/'

# Commencer par utiliser alpine. Elle n'a pas de variables catégorielles.
# Vous pouvez en rajouter vous même !


class Alpine1:
    '''
    Alpine1 function
    Only numerical inputs
    '''

    def __init__(self, input_dim):
        self.input_dim = input_dim
        self.domain = [{
            'name': 'V' + str(i),
            'type': 'continuous',
            'domain': (-10, 10),
            'dimensionality': 1
        } for i in range(self.input_dim)]


    def f(self, X):
        if X.ndim == 1:
            n = 1
            X = X.reshape(1, self.input_dim)
        n = X.shape[0]
        fval = np.abs(X * np.sin(X) + 0.1 * X).sum(axis=1)
        return fval.reshape(n, 1)



'''
    Base class for simulations.
    Each simulations is based on a ML model :
        - RF = Random Forest
        - SVR = Support Vector Machine (Regression)
'''
class Simulation:

    def __init__(self, input_dim, dim_continuous, cat_choices):
        self.dim_continuous = dim_continuous
        self.input_dim = input_dim
        self.arr_dim_continuous = [i for i in range(self.input_dim) if i < self.dim_continuous]
        self.cat_choices = cat_choices
        self.arr_cat_dim = [i for i in range(self.input_dim) if i >= self.dim_continuous]


    '''
    Zip inputs to be readable by the simulation.
    - One hot encoding for categorical variables
    '''

    def zip_inputs(self, X):
        n = X.shape[0]
        res = np.zeros((n, self.dim_continuous))
        res[:, self.arr_dim_continuous] = X[:, self.arr_dim_continuous]
        categorical = []
        for cat in self.cat_choices:
            categorical.append(np.zeros((n, cat)))
        k = 0
        for x in X:
            i = 0
            for cat in self.cat_choices:
                categorical[i][k, int(x[self.arr_cat_dim[i]])] += 1
                i += 1
        for c in categorical:
            res = np.append(res, c, axis=1)
        return res

    def unzip_inputs(self, X):
        if X.ndim == 1:
            n = 1
            X = X.reshape(1, -1)
        else:
            n = X.shape[0]
        res = np.append(X[:, self.arr_dim_continuous], np.zeros((n, len(self.arr_cat_dim))), axis=1)
        for i in range(n):
            k = 0
            for cat_idx in self.arr_cat_dim:
                res[i, cat_idx] += np.argmax(X[i, self.cat_choices[k]])
                k += 1
        return res


    def f(self, X):
        if X.ndim == 1:
            X = X.reshape(1, -1)
        n = X.shape[0]
        dim = X.shape[1]
        if dim == self.input_dim:
            X_zipped = self.zip_inputs(X)
        else:
            X_zipped = X
        fval = self.model.predict(X_zipped)
        fval = fval.reshape(n, 1)
        return - fval.item()  # Minimisation


class Reizman1(Simulation):
    '''
        Reizman case 1 - "3-bromoquinoline"
        See supplementary information

        ** Suzuki–Miyaura cross-coupling optimization enabled by automated feedback -
        Brandon J. Reizman, Yi-Ming Wang, Stephen L. Buchwald  and  Klavs F. Jensen  **
    '''

    def __init__(self):
        super().__init__(input_dim=4, dim_continuous=3, cat_choices=[8])
        model_path = "SVR_reizman1.sav"
        self.model = pickle.load(open(savings_path + model_path, 'rb'))
        self.idx_cat = [3]
        self.domain = [
            {'name': 'Tres', 'type': 'continuous', 'domain': (0, 1), 'dimensionality': 1},
            {'name': 'T°C', 'type': 'continuous', 'domain': (0, 1), 'dimensionality': 1},
            {'name': 'CatLoading', 'type': 'continuous', 'domain': (0, 1), 'dimensionality': 1},
            {'name': 'Catalyst', 'type': 'categorical', 'domain': (0, 1, 2, 3, 4, 5, 6, 7), 'dimensionality': 1}
        ]



class DirectArylation(Simulation):
    '''
    Pd-Catalysed Direct Arylation

    ** Shields, B. J., Stevens, J., Li, J., Parasram, M., Damani, F., Alvarado,
    J. I. M., ... & Doyle, A. G. (2021).
    Bayesian reaction optimization as a tool for chemical synthesis. Nature. **
    '''
    def __init__(self):
        super().__init__(input_dim=5, dim_continuous=2, cat_choices=[4, 12, 4])
        model_path = "RF_direct_arylation.sav"
        self.model = pickle.load(open(savings_path + model_path, 'rb'))
        self.idx_cat = [2, 3, 4]
        self.domain = [
            {'name': 'concentration', 'type': 'continuous', 'domain': (0, 1), 'dimensionality': 1},
            {'name': 'Temperature', 'type': 'continuous', 'domain': (0, 1), 'dimensionality': 1},
            {'name': 'base', 'type': 'categorical', 'domain': (0, 1, 2, 3), 'dimensionality': 1},
            {'name': 'ligand', 'type': 'categorical', 'domain': (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11),
             'dimensionality': 1},
            {'name': 'solvent', 'type': 'categorical', 'domain': (0, 1, 2, 3), 'dimensionality': 1}
        ]
