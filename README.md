# POM-Starter

## Introduction

Ce projet est une introduction à un environnement de recherche. Vous vous situez dans l'optimisation de boite noire avec variables mixtes.
Un problème d'optimisation se résume à trouver la (ou les) configuration X qui donne le meilleur résultat Y. Dans notre cas, X est un ensemble de variables de types mixtes (catégoriel, continu).
Le machine learning intervient dans l'optimisation moderne, il est principalement utilisé en tant qu'outil. Vous allez donc vous frotter à des algorithmes de ML, sans pour autant les implémenter de toute pièce (bonne introduction au ML).
Certains algorithmes proposés ne sont pas adaptés pour les variables mixtes, ce n'est pas un soucis, si c'est votre choix, on s'adaptera.


### TODO
Vous allez devoir :
- Choisir un papier de la "pool"
- Faire un petit résumé (1-2 pages) et après une petite présentation (3/4 slides). On fixera les dates ensemble, si vous voulez assister aux autres présentations, vous pourrez.
- Implémenter ou utiliser l'algorithme choisi
- Montrer des résultats d'optimisation (courbe de convergence, de regret, ...)
- Si vous pensez avoir fini, vous pouvez, au choix:
    - choisir un autre algorithme, proposer une implémentation ou une utilisation
    - ajouter de nouvelles fonctionnalités / améliorations

### Matériel
Vous trouverez dans l'archive :
- Un ensemble d'article
- Les fonctions boites noires à optimiser
- Un exemple d'utilisation des fonctions boites noires avec une stratégie aléatoire
- Un template latex
- Un ensemble de dataset représentant des réactions chimiques à variables mixtes
Il est possible d'avoir accès à une VM.


### Algorithmes d'optimisation :

Voici les 5 approches proposées avec leurs mots clés:

- Apprentissage profond par renforcement pour l'optimisation (DRO) - Deep Learning, Reinforcement learning, pre-training
- Optimisation bayésienne avec gradient boosting (hyperboost) - Machine learning, ensemble models, bayesianisme, AutoML
- Optimisation bayésienne avec TPE (TPE-BO) - Machine learning, ensemble models, bayesianisme, AutoML
- Optimisation avec colonie de fourmis (ACO)** - AI, bio-inspired intelligence, evolutionnary algorithm, heuristics
- Optimisation avec essaim de particules (PSO)** - AI, bio-inspired intelligence, evolutionnary algorithm, heuristics



** = Implémentation obligatoire


### Instructions de développement:

- Python 3.7 ou 3.8
- Executable soit en ligne de commande, soit avec un IDE (pycharm par exemple)
- numpy, sklearn, scipy, matplotlib, pandas, jupyter ...


### Détails
- Pour les binômes, au moins une implémentation vous sera demandée
- Donner nous des accès à vos repos gitlab. Nous vérifierons l'avancement de vos travaux et cela nous permettra de discuter de vos implémentations plus simplement
- Partez du principe que si vous en faites plus, vous allez avoir plus. (note mais aussi découverte et compréhension de l'environnement)
- Pour la communication, selon ce que vous préférez, il y aura un discord ou un slack disponible
- Vos résumés et présentations devront être réalisé avec latex. Vous trouverez un template dans le repo. TexMaker et Overleaf sont des bons outils pour produire des documents avec latex
- Toutes les suggestions ou prise d'initiative sont les bienvenues (autre algorithme par exemple).
